﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DataBase.DtoModels;

namespace DataBase.Repositories
{
	/// <summary>
	/// Generic repository interface 
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public interface IRepository<TEntity> where TEntity : IEntity
	{
		/// <summary>
		/// Create new entity in DB
		/// </summary>
		/// <param name="entity"></param>
		void Create(TEntity entity);

		/// <summary>
		/// Delete entity from DB by entity model
		/// </summary>
		/// <param name="entity"></param>
		void Delete(TEntity entity);

		/// <summary>
		/// Delete entity from DB by id
		/// </summary>
		/// <param name="id"></param>
		void Delete(long id);

		/// <summary>
		/// Update entity in DB
		/// </summary>
		/// <param name="entity"></param>
		void Update(TEntity entity);

		/// <summary>
		/// Get List Entities
		/// </summary>
		/// <returns></returns>
		List<IGrouping<TGroup, TEntity>> Get<TKey, TGroup>(
			Expression<Func<TEntity, bool>> predicates,
			Expression<Func<TEntity, TKey>> orderSelector,
			Func<TEntity, TGroup> groupSelector
		);

		/// <summary>
		/// Get List Entities
		/// </summary>
		/// <returns></returns>
		IEnumerable<TEntity> Get();

		/// <summary>
		/// Get Entity by ID
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		TEntity GetById(long id);
	}
}
