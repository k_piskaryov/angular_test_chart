﻿namespace DataBase.DtoModels
{
	/// <summary>
	/// Generic Model in DB 
	/// </summary>
	public interface IEntity
	{
		/// <summary>
		/// Model Identity
		/// </summary>
		long Id { get; set; }
	}
}
