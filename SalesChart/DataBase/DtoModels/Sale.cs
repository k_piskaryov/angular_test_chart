﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataBase.DtoModels
{
	/// <summary>
	/// Model Sale in DataBase
	/// </summary>
	public class Sale : IEntity
	{
		/// <summary>
		/// Model Identity
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }
		
		/// <summary>
		/// Sum
		/// </summary>
		[Required]
		public double Sum { get; set; }

		/// <summary>
		/// Date
		/// </summary>
		[Required]
		public DateTime Date { get; set; }
	}
}
