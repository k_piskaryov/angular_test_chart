﻿using DataBase.DtoModels;
using Microsoft.EntityFrameworkCore;

namespace DataBase
{
	/// <summary>
	/// Sale context for work with DB
	/// </summary>
	public class SaleContext : DbContext
	{
		public DbSet<Sale> Sales { get; set; }

		public SaleContext(DbContextOptions options)
			: base(options)
		{
		}
	}
}
