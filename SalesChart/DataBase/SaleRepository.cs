﻿using DataBase.DtoModels;

namespace DataBase
{
	public class SaleRepository : Repository<Sale>
	{
		public SaleRepository(SaleContext context) :
			base(context)
		{
		}
	}
}