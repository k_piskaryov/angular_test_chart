﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DataBase.DtoModels;
using DataBase.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DataBase
{
	/// <summary>
	/// Generic Repository
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public class Repository<TEntity> : IRepository<TEntity>
		where TEntity : class, IEntity
	{
		private readonly SaleContext _context;

		public Repository(SaleContext context) => _context = context;

		public void Create(TEntity entity)
		{
			_context.Set<TEntity>().Add(entity);
			_context.SaveChanges();
		}

		public void Delete(TEntity entity)
		{
			_context.Set<TEntity>().Remove(entity);
			_context.SaveChanges();
		}

		public void Delete(long id)
		{
			var entityToDelete = _context.Set<TEntity>().FirstOrDefault(e => e.Id == id);
			if (entityToDelete != null)
			{
				_context.Set<TEntity>().Remove(entityToDelete);
			}
			_context.SaveChanges();
		}

		public void Update(TEntity entity)
		{
			var editedEntity = _context.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
			if (editedEntity != null)
			{
				_context.Entry(editedEntity).State = EntityState.Detached;
			}
			editedEntity = entity;
			_context.Entry(editedEntity).State = EntityState.Modified;
			_context.SaveChanges();
		}

		public List<IGrouping<TGroup, TEntity>> Get<TKey, TGroup>(
			Expression<Func<TEntity, bool>> predicates,
			Expression<Func<TEntity, TKey>> orderSelector,
			Func<TEntity, TGroup> groupSelector
			)
		{
			return _context.Set<TEntity>()
				.Where(predicates)
				.OrderBy(orderSelector)
				.GroupBy(groupSelector).ToList();
		}

		public IEnumerable<TEntity> Get()
		{
			return _context.Set<TEntity>().ToList();
		}

		public TEntity GetById(long id)
		{
			return _context.Set<TEntity>().FirstOrDefault(e => e.Id == id);
		}
	}
}
