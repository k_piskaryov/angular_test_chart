import Grouptype = require("./group-type");
import GroupType = Grouptype.GroupType;

export class  ChartResponse {
  year: number;
  period: number;
  sum: number;
  count: number;
  groupType: GroupType;
}
