import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ChartResponse } from "../models/chart-response";
import { GroupType } from "../models/group-type";

@Injectable()
export class SaleDataService {
  private baseUrl: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  getChartInfo(): Observable<ChartResponse[]> {
    const params = new HttpParams()
      .set('startDate', '2020-07-15')
      .set('endDate', '2020-07-16')
      .set('type', GroupType.Month.toString());
    return this.http.get<ChartResponse[]>(this.baseUrl + 'api/Sale/sales', { params });
  };
}
