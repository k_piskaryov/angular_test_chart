import { Component, OnInit } from '@angular/core';
import { SaleDataService } from '../services/sale-data.service';
import { ChartResponse } from "../models/chart-response";

@Component(
  {
    selector: 'app-home',
    templateUrl: './home.component.html',
    providers: [SaleDataService]
  })
export class HomeComponent implements OnInit {

  public chartInfos: ChartResponse[];

  constructor(private saleDataService: SaleDataService) {
  }

  ngOnInit() {
    this.saleDataService.getChartInfo().subscribe(
      result => {
        this.chartInfos = result;
      },
      error => console.error(error));
  }
}
