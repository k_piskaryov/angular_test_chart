﻿using System;

namespace SalesChart.Requests
{
	/// <summary>
	/// Sale response
	/// </summary>
	public class SaleResponse
	{
		/// <summary>
		/// Model Identity
		/// </summary>
		public long SaleId { get; set; }

		/// <summary>
		/// Sum
		/// </summary>
		public double Sum { get; set; }

		/// <summary>
		/// Date
		/// </summary>
		public DateTime Date { get; set; }
	}
}
