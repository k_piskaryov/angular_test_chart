﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SalesChart.Requests
{
	/// <summary>
	/// Sale request
	/// </summary>
	public class SaleRequest
	{
		/// <summary>
		/// Sum
		/// </summary>
		[Required]
		public double Sum { get; set; }

		/// <summary>
		/// Date
		/// </summary>
		[Required]
		public DateTime Date { get; set; }
	}
}
