﻿namespace SalesChart.Requests
{
	public enum GroupType
	{
		Quarter,
		Month,
		Week,
		Day
	}
}
