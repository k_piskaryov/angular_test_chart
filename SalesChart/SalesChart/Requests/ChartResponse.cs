﻿namespace SalesChart.Requests
{
	public class ChartResponse
	{
		public int Year { get; set; }

		public int Period { get; set; }

		public double Sum { get; set; }

		public int Count { get; set; }

		public GroupType GroupType { get; set; }
	}
}
