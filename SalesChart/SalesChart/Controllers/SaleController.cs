﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using SalesChart.Requests;
using SalesChart.Services;

namespace SalesChart.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class SaleController : ControllerBase
	{

		private readonly ISaleService _saleService;

		public SaleController(ISaleService saleService)
		{
			_saleService = saleService;
		}

		/// <summary>
		/// Get ChartResponse List
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route("sales")]
		[ResponseType(typeof(IEnumerable<ChartResponse>))]
		public ActionResult<IEnumerable<ChartResponse>> GetSales([FromQuery] DateTime startDate, [FromQuery] DateTime endDate, [FromQuery] GroupType type = GroupType.Quarter)
		{
			try
			{
				var sales = _saleService.GetSales(startDate, endDate, type);
				return sales == null ? NotFound() : (ActionResult<IEnumerable<ChartResponse>>)Ok(sales);
			}
			catch (Exception)
			{
				return BadRequest();
			}
		}

		/// <summary>
		/// Add new Sale
		/// </summary>
		/// <param name="data">object for add new Sale</param>
		/// <returns></returns>
		[HttpPost]
		[Route("sale")]
		public ActionResult CreateSale([FromBody] SaleRequest data)
		{
			var result = _saleService.CreateSale(data);
			if (result <= 0)
			{
				return BadRequest();
			}

			return Ok(result);
		}
	}
}
