﻿using System;
using System.Collections.Generic;
using SalesChart.Requests;

namespace SalesChart.Services
{
	/// <summary>
	/// interface service for operation with Sale
	/// </summary>
	public interface ISaleService
	{
		/// <summary>
		/// Create new Sale in DB
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		long CreateSale(SaleRequest data);

		/// <summary>
		/// Get List Sales
		/// </summary>
		/// <returns></returns>
		IEnumerable<SaleResponse> GetSales();

		/// <summary>
		/// Get List ChartResponse by params
		/// </summary>
		/// <returns></returns>
		IEnumerable<ChartResponse> GetSales(DateTime startDate, DateTime endDate, GroupType type);

		/// <summary>
		/// Get Sale by Id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		SaleResponse GetSale(long id);
	}
}
