﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataBase;
using DataBase.DtoModels;
using Microsoft.Extensions.Logging;
using SalesChart.Requests;

namespace SalesChart.Services
{
	public class SaleService: ISaleService
	{
		private readonly SaleRepository _saleRepository;
		private readonly ILogger _logger;

		public SaleService(SaleRepository saleRepository, ILogger<SaleService> logger)
		{
			_saleRepository = saleRepository;
			_logger = logger;
		}
		
		public long CreateSale(SaleRequest data)
		{
			try
			{
				var sale = GetSaleByRequest(data);
				_saleRepository.Create(sale);
				return sale.Id;
			}
			catch (Exception ex)
			{
				_logger.LogWarning(ex, "CreateSale error");
				return 0;
			}
		}

		public IEnumerable<SaleResponse> GetSales()
		{
			var defaultResult = new List<SaleResponse>();
			try
			{
				var books = _saleRepository.Get();
				defaultResult = books.Select(GetSaleResponseByModel).ToList();
			}
			catch (Exception ex)
			{
				_logger.LogWarning(ex, "GetSales error");
			}

			return defaultResult;
		}

		public IEnumerable<ChartResponse> GetSales(DateTime startDate, DateTime endDate, GroupType type)
		{
			var defaultResult = new List<ChartResponse>();
			try
			{
				var books = _saleRepository
					.Get(s => s.Date >= startDate && s.Date < endDate,
						s => s.Date,
						gr => new
						{
							Year = gr.Date.Year,
							Period = gr.Date.Month
						});
				
				defaultResult = books.Select(gr => new ChartResponse
				{
					Count = gr.Count(),
					Sum = gr.Sum(s=>s.Sum),
					Year = gr.Key.Year,
					Period = gr.Key.Period,
					GroupType = type
				}).ToList();
			}
			catch (Exception ex)
			{
				_logger.LogWarning(ex, "GetSales error");
			}

			return defaultResult;
		}

		public SaleResponse GetSale(long id)
		{
			throw new NotImplementedException();
		}


		private Sale GetSaleByRequest(SaleRequest data)
		{
			return new Sale
			{
				Id = 0,
				Sum= data.Sum,
				Date = data.Date
			};
		}

		private SaleResponse GetSaleResponseByModel(Sale data)
		{
			return new SaleResponse
			{
				SaleId = 0,
				Sum = data.Sum,
				Date = data.Date
			};
		}
	}
}
